.section .text
.extern _GetStdHandle@4

.globl _load_streams
.globl _load_stdout

_load_streams:
	pushl $-11
	calll _GetStdHandle@4
	movl %eax, stdout_stream
	ret

_load_stdout:
	movl stdout_stream, %eax
	ret

.section .data
stdout_stream:
	.long 0
