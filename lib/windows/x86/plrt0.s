.section .text
.extern _main
.extern _ExitProcess@4
.extern _initlib

.globl _start

_start:
	call _initlib
	call _main
	# eax = return code
	pushl %eax
	calll _ExitProcess@4
