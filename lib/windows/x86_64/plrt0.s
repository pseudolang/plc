.section .text
.extern _main
.extern _ExitProcess@8
.extern _initlib

.globl _start

_start:
	callq _initlib
	callq _main
	# eax = return code
	pushq %rax
	callq _ExitProcess@8
