.section .text
.globl _initlib

.extern _load_streams

_initlib:
	calll _load_streams
	ret
