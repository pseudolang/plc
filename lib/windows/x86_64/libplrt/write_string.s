.section .text
.extern _load_stdout
.extern _WriteFile@20

.globl _write_string
_write_string:
	pushl $0
	leal -4(%esp), %ecx
	pushl %ecx
	pushl %ebx
	pushl %eax
	calll _load_stdout
	pushl %eax
	calll _WriteFile@20
	ret
