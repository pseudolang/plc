.section .text
.global write_string
.type write_string, @function
write_string:
	movq %rax, %rsi
	movq %rbx, %rdx
	movq $1, %rax
	movq $1, %rdi
	syscall
	ret
