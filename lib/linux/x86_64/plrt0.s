.section .text
.extern main
.global _start
.type _start, @function

_start:
	callq main
	# rax = return code
	movq %rax, %rdi
	movq $60, %rax
	syscall
