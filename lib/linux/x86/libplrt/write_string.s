.section .text
.global write_string
.type write_string, @function
write_string:
	movl %eax, %ecx
	movl %ebx, %edx
	movl $4, %eax
	movl $1, %ebx
	int $0x80
	ret
