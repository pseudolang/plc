.section .text
.extern main
.global _start
.type _start, @function

_start:
	call main
	# eax = return code
	movl %eax, %ebx
	movl $1, %eax
	int $0x80
