# PLC
This is a compiler for a toy language that I call
Pseudo Lang, PLang or simply PL. It is a language I created
for the sake of learning how compilers work;
still early in development.

## Target platforms
The compiler backend will support Linux (i386 and x86_64) and Windows (i386 and x86_64)

## Contributing
If you would like to contribute, please open a merge request on 
[this](https://gitlab.com/pseudolang/plc) repo.
Use **meaningful** commit messages.

The coding standard that should be used is the one described
[here](https://llvm.org/docs/CodingStandards.html).

**The code should be self-explanatory; if it isn't you should document changes**.

## Support
You may join the official [PL Discord Server](https://discord.gg/2J6zZgH)
to ask for help or make a feature request.