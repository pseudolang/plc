#pragma once
#include <string>
namespace plc {
namespace os {
enum Os {
  LINUX, WINDOWS
};

std::string matchAbi(Os Os, std::string Name);
}
}