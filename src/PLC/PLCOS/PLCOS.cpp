#include "PLCOS.hpp"

std::string plc::os::matchAbi(plc::os::Os Os, std::string Name) {
  if (Os == plc::os::Os::WINDOWS) {
	return "_" + Name;
  } else {
	return Name;
  }
}