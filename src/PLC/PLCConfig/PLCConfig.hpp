#pragma once
#include <fstream>
#include <memory>
#include <PLC/PLCOS/PLCOS.hpp>

namespace plc {
class Config {
public:
  size_t getLangMajorVersion() const;
  size_t getLangMinorVersion() const;
  size_t getLangPatchLevel() const;
  friend std::shared_ptr<Config> parseConfig(std::fstream &ConfigIn);

  plc::os::Os getTargetOs() const;
private:
  size_t m_LangVersion; // major * 10000 + minor * 100 + patch
  plc::os::Os m_TargetOS;
};

std::shared_ptr<Config> parseConfig(std::fstream &ConfigIn);

void parseConfigLineArgs(std::shared_ptr<Config> Config, int &Argc, char **Argv);
}
