#include <PLC/PLCConfig/PLCConfig.hpp>
#include <PLC/PLCUtils/PLCStringUtils.hpp>

size_t plc::Config::getLangMajorVersion() const {
  return this->m_LangVersion / 10000;
}

size_t plc::Config::getLangMinorVersion() const {
  return (this->m_LangVersion / 100) % 100;
}

size_t plc::Config::getLangPatchLevel() const {
  return this->m_LangVersion % 100;
}

std::shared_ptr<plc::Config> plc::parseConfig(std::fstream &ConfigIn) {
  std::shared_ptr<plc::Config> Config = std::make_shared<plc::Config>();
  std::string ConfigLine;
  while (std::getline(ConfigIn, ConfigLine)) {
	size_t IndexOfComment = ConfigLine.find('#');
	if (IndexOfComment != std::string::npos) {
	  ConfigLine = ConfigLine.substr(0, IndexOfComment);
	}
	size_t EqIndex = ConfigLine.find('=');
	std::string Lhs = ConfigLine.substr(0, EqIndex);
	std::string Rhs = ConfigLine.substr(EqIndex + 1);
	plc::trimString(Lhs);
	plc::trimString(Rhs);

	if (Lhs == "LangVer") {
	  Config->m_LangVersion = plc::parseVersion(Rhs);
	} else if (Lhs == "TargetOS") {
	  if (Rhs == "linux") {
		Config->m_TargetOS = plc::os::Os::LINUX;
	  } else if (Rhs == "win") {
		Config->m_TargetOS = plc::os::Os::WINDOWS;
	  }
	}
  }
  return Config;
}
plc::os::Os plc::Config::getTargetOs() const {
  return this->m_TargetOS;
}

void plc::parseConfigLineArgs(std::shared_ptr<plc::Config> Config, int &Argc, char **Argv) {
}
