#include <PLC/PLCAST/PLCASTNode.hpp>
#include <iostream>
#include <PLC/PLCLog/PLCLog.hpp>

plc::ast::Node::Node(plc::ast::NodeType Type) : m_Type(Type) {}

plc::ast::Node::Node(plc::ast::NodeType Type, plc::lex::Token Token) : m_Type(Type), m_Token(Token) {}

plc::ast::Node *plc::ast::Node::getParent() {
  return this->m_Parent;
}

std::shared_ptr<plc::ast::Node> plc::ast::Node::getNextSibling() {
  return this->m_NextSibling;
}

std::shared_ptr<plc::ast::Node> plc::ast::Node::getFirstChild() {
  return this->m_Child;
}

void plc::ast::Node::addChild(std::shared_ptr<Node> Child) {
  if (this->m_Child == nullptr) {
	this->m_Child = Child;
	this->m_LastChild = Child.get();
	Child->setReferer(&this->m_Child);
  } else {
	this->m_LastChild->m_NextSibling = Child;
	Child->setReferer(&this->m_LastChild->m_NextSibling);
	this->m_LastChild = Child.get();
  }
  this->m_LastChild->m_Parent = this;
  while (this->m_LastChild->m_NextSibling != nullptr) {
	this->m_LastChild = this->m_LastChild->m_NextSibling.get();
	this->m_LastChild->m_Parent = this;
  }
}

plc::ast::NodeType plc::ast::Node::getNodeType() {
  return this->m_Type;
}

std::optional<plc::lex::Token> plc::ast::Node::getToken() {
  return this->m_Token;
}

void plc::ast::Node::print() {
  std::string NodeTypeName = "";
  switch (this->m_Type) {
  case plc::ast::NodeType::PARENTHESIS_NODE: {
	NodeTypeName = "par_node";
	break;
  }
  case plc::ast::NodeType::SCOPE_NODE: {
	NodeTypeName = "scope_node";
	break;
  }
  case plc::ast::NodeType::FUNCTION_CALL_NODE: {
	NodeTypeName = "function_call_node";
	break;
  }
  case plc::ast::NodeType::FUNCTION_CALL_NAME_NODE: {
	NodeTypeName = "function_call_name_node";
	break;
  }
  case plc::ast::NodeType::FUNCTION_CALL_ARG_NODE: {
	NodeTypeName = "function_call_arg_node";
	break;
  }
  case plc::ast::NodeType::IDENTIFIER_NODE: {
	NodeTypeName = "identifier_node";
	break;
  }
  case plc::ast::NodeType::PARENTHESIS_OPENED_NODE: {
	NodeTypeName = "par_open_node";
	break;
  }
  case plc::ast::NodeType::PARENTHESIS_CLOSED_NODE: {
	NodeTypeName = "par_close_node";
	break;
  }
  case CONSTANT_STRING_NODE: {
	NodeTypeName = "constant_string_node";
	break;
  }
  }
  plc::log(plc::LogSeverity::INFO, "%s", NodeTypeName.c_str());
  if (this->m_Child != nullptr) {
	plc::log(plc::LogSeverity::INFO, "Children:");
	this->m_Child->print();
	plc::log(plc::LogSeverity::INFO, "End of Children list");
  }
  if (this->m_NextSibling != nullptr) {
	this->m_NextSibling->print();
  }
}
void plc::ast::Node::setReferer(std::shared_ptr<plc::ast::Node> *Referer) {
  this->m_Referer = Referer;
}

// documentation for this function is found in the readme
void plc::ast::Node::parentify(std::shared_ptr<plc::ast::Node> Parent, std::size_t Size) {
  if (this->m_Referer != nullptr) {
    Parent->m_Referer = this->m_Referer;
    *Parent->m_Referer = Parent;
  }
  this->m_Referer = &Parent->m_Child;
  Parent->m_Child = std::make_shared<plc::ast::Node>(*this);
  Parent->m_Parent = this->m_Parent;
  if(this->m_Parent->m_LastChild == this){
	this->m_Parent->m_LastChild = Parent.get();
  }
  this->m_Parent = Parent.get();
  if (Size > 0) {
	std::shared_ptr<plc::ast::Node> Node = this->m_NextSibling;
	for (std::size_t I = 0; I < Size - 1; I++) {
	  Node->m_Parent = Parent.get();
	  Node = Node->m_NextSibling;
	}
	if(Node->m_Parent->m_LastChild == Node.get()){
	  Node->m_Parent->m_LastChild = Parent.get();
	}
	Node->m_Parent = Parent.get();
	Parent->m_NextSibling = Node->m_NextSibling;
	if (Node->m_NextSibling != nullptr)
	  Node->m_NextSibling->m_Referer = &Parent->m_NextSibling;
	Node->m_NextSibling = nullptr;
  } else {
	this->m_NextSibling->m_Referer = &Parent->m_NextSibling;
	Parent->m_NextSibling = this->m_NextSibling;
	this->m_NextSibling = nullptr;
  }
}

void plc::ast::Node::setType(plc::ast::NodeType Type) {
  this->m_Type = Type;
}
