#pragma once

#include <optional>
#include <memory>
#include <PLC/PLCLex/PLCToken.hpp>

namespace plc {
namespace ast {
enum NodeType {
  SCOPE_NODE,
  FUNCTION_CALL_NODE,
  FUNCTION_CALL_NAME_NODE,
  FUNCTION_CALL_ARG_NODE,
  IDENTIFIER_NODE,
  PARENTHESIS_NODE,
  PARENTHESIS_OPENED_NODE,
  PARENTHESIS_CLOSED_NODE,
  CONSTANT_STRING_NODE
};

class Node {
public:
  Node(NodeType Type);
  Node(NodeType Type, plc::lex::Token Token);
  virtual ~Node() = default;

  Node *getParent();

  std::shared_ptr<Node> getNextSibling();

  std::shared_ptr<Node> getFirstChild();
  void addChild(std::shared_ptr<Node> Child);

  NodeType getNodeType();

  std::optional<plc::lex::Token> getToken();

  void print();

  void setReferer(std::shared_ptr<Node> *Referer);

  // documentation for this function is found in the readme
  void parentify(std::shared_ptr<Node> Parent, std::size_t Size);

  void setType(NodeType Type);
private:
  Node *m_Parent;
  std::shared_ptr<Node> m_Child;
  Node *m_LastChild;
  std::shared_ptr<Node> m_NextSibling;

  NodeType m_Type;
  std::optional<plc::lex::Token> m_Token;
  std::shared_ptr<Node> *m_Referer = nullptr;
};
}
}