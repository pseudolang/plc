# PLC AST
This subdirectory contains files that are used to represent the abstract syntax tree.

## plc::ast::Node::parentify(parent, n)
This function moves `this` and the next `n` nodes on the level `this` is on to the next level of the AST,
with the parameter `parent` replacing them all on that level and each node's parent is set to `parent`.

Example:

Before:

![](../../../assets/parentify_before.png)

```c
D.parentify(Node(G), 1);
```

After:

![](../../../assets/parentify_after.png)