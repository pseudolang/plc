#pragma once
#include <string>
namespace plc {
namespace plc_asm {
class AsmLabel {
  AsmLabel(std::string Name);
private:
  std::string m_Name;
public:
  std::string getName();
};
}
}