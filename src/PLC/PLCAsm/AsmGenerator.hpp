#pragma once
#include <PLC/PLCIR/PLCIR.hpp>
#include <PLC/PLCAsm/AsmInstruction.hpp>
#include <functional>
#include <memory>
#include <PLC/PLCAsm/AsmLabel.hpp>
#include <PLC/PLCConfig/PLCConfig.hpp>
namespace plc {
namespace plc_asm {
class AsmGenerator {
public:
  AsmGenerator(plc::ir::IR Ir, const plc::Config &Config) : m_Ir(Ir), m_Config(Config) {};
  virtual void generateAssembly() = 0;

  const std::vector<std::variant<std::shared_ptr<plc::plc_asm::AsmInstruction>,
								 std::shared_ptr<plc::plc_asm::AsmLabel> > > getInstructions() {
	return this->m_AsmCode;
  }

  inline const plc::ir::IR &getIr() const { return m_Ir; }

protected:
  plc::ir::IR m_Ir;
  const plc::Config &m_Config;
  std::vector<std::variant<std::shared_ptr<plc::plc_asm::AsmInstruction>, std::shared_ptr<plc::plc_asm::AsmLabel> > >
	  m_AsmCode;
};
}
}