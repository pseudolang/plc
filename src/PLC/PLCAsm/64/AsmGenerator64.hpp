#pragma once
#include <PLC/PLCAsm/AsmGenerator.hpp>
namespace plc {
namespace plc_asm {
namespace x_64 {
class AsmGenerator64 : public plc::plc_asm::AsmGenerator {
public:
  AsmGenerator64(plc::ir::IR Ir, const plc::Config &Config);
  void generateAssembly() override;
};

}
}
}