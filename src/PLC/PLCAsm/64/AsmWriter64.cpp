#include <PLC/PLCAsm/64/AsmWriter64.hpp>
plc::plc_asm::x_64::AsmWriter64::AsmWriter64(plc::plc_asm::AsmGenerator &GeneratorResult,
											 std::ostream &Fout,
											 const plc::Config &Config) : plc::plc_asm::AsmWriter(GeneratorResult,
																								  Fout,
																								  Config) {}

void plc::plc_asm::x_64::AsmWriter64::write() {
  std::string MainName = plc::os::matchAbi(m_Config.getTargetOs(), "main");
  m_FileOut << ".section .text\n.globl " << MainName << "\n" << MainName << ":\n";
  for (const auto &Instruction : m_GeneratorResult.getInstructions()) {
	std::visit([=](auto &&Arg) {
	  using T = std::decay_t<decltype(Arg)>;
	  if constexpr (std::is_same_v<T, std::shared_ptr<plc::plc_asm::AsmInstruction> >) {
		this->m_FileOut << static_cast<std::shared_ptr<plc::plc_asm::AsmInstruction> >(Arg)->str() << "\n";
	  } else if constexpr (std::is_same_v<T, std::shared_ptr<plc::plc_asm::AsmLabel> >) {
		this->m_FileOut << static_cast<std::shared_ptr<plc::plc_asm::AsmLabel> >(Arg)->getName() << ":\n";
	  }
	}, Instruction);
  }
  m_FileOut << "movq $0, %rax\nret\n.section .rodata\n";
  for (const auto &Str : m_GeneratorResult.getIr().getConstStrings()) {
	m_FileOut << Str.getName() << ": .ascii \"" << Str.getValue() << "\"\n";
  }
}
