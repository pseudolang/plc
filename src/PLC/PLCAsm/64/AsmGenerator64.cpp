#include <iostream>
#include <PLC/PLCAsm/64/AsmGenerator64.hpp>

plc::plc_asm::x_64::AsmGenerator64::AsmGenerator64(plc::ir::IR Ir, const plc::Config &Config) : AsmGenerator(Ir,
																											 Config) {}

void plc::plc_asm::x_64::AsmGenerator64::generateAssembly() {
  ir::IR Ir = this->m_Ir;
  for (const plc::ir::Instruction &Instr : Ir.getInstructions()) {
	if (Instr.getInstr() == plc::ir::InstrEnum::LOAD_STR_CONST) {
	  std::string StrVarName(std::get<std::string>(Instr.getArgs()[0]));
	  plc::plc_asm::AsmInstruction AsmInstr("leaq", {StrVarName, "%rax"});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	  AsmInstr = plc::plc_asm::AsmInstruction("movq", {"$" + std::to_string(Ir.constStringLen(StrVarName)), "%rbx"});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	} else if (Instr.getInstr() == plc::ir::InstrEnum::API_CALL) {
	  plc::plc_asm::AsmInstruction
		  AsmInstr("callq", {plc::os::matchAbi(m_Config.getTargetOs(), std::get<std::string>(Instr.getArgs()[0]))});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	}
  }
}