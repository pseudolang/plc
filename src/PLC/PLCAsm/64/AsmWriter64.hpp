#pragma once
#include <PLC/PLCAsm/AsmWriter.hpp>

namespace plc {
namespace plc_asm {
namespace x_64 {

class AsmWriter64 : public plc::plc_asm::AsmWriter {
public:
  AsmWriter64(AsmGenerator &GeneratorResult, std::ostream &Fout, const plc::Config &Config);
  void write() override;
};

}
}
}