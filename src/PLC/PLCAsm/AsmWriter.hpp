#pragma once
#include <PLC/PLCAsm/AsmGenerator.hpp>

#include <ostream>

namespace plc {
namespace plc_asm {

class AsmWriter {
public:
  AsmWriter(AsmGenerator &Generator, std::ostream &Fout, const plc::Config &Config)
	  : m_GeneratorResult(Generator), m_FileOut(Fout), m_Config(Config) {}
  virtual void write() = 0;
protected:
  AsmGenerator &m_GeneratorResult;
  std::ostream &m_FileOut;
  const plc::Config &m_Config;
};

}
}
