#pragma once

#include <PLC/PLCAsm/AsmWriter.hpp>

namespace plc {
namespace plc_asm {
namespace x_86 {

class AsmWriter86 : public plc::plc_asm::AsmWriter {
public:
  AsmWriter86(AsmGenerator &GeneratorResult, std::ostream &Fout, const plc::Config &Config);
  void write() override;
};

}
}
}