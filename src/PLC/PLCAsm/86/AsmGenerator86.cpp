#include "AsmGenerator86.hpp"
plc::plc_asm::x_86::AsmGenerator86::AsmGenerator86(plc::ir::IR Ir, const plc::Config &Config) : AsmGenerator(Ir,
																											 Config) {}

void plc::plc_asm::x_86::AsmGenerator86::generateAssembly() {
  ir::IR Ir = this->m_Ir;
  for (const plc::ir::Instruction &Instr : Ir.getInstructions()) {
	if (Instr.getInstr() == plc::ir::InstrEnum::LOAD_STR_CONST) {
	  std::string StrVarName(std::get<std::string>(Instr.getArgs()[0]));
	  plc::plc_asm::AsmInstruction AsmInstr("leal", {StrVarName, "%eax"});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	  AsmInstr = plc::plc_asm::AsmInstruction("movl", {"$" + std::to_string(Ir.constStringLen(StrVarName)), "%ebx"});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	} else if (Instr.getInstr() == plc::ir::InstrEnum::API_CALL) {
	  plc::plc_asm::AsmInstruction
		  AsmInstr("calll", {plc::os::matchAbi(m_Config.getTargetOs(), std::get<std::string>(Instr.getArgs()[0]))});
	  this->m_AsmCode.push_back(std::make_shared<plc::plc_asm::AsmInstruction>(AsmInstr));
	}
  }
}
