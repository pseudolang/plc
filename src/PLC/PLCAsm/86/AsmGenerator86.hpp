#pragma once
#include <PLC/PLCAsm/AsmGenerator.hpp>
#include <PLC/PLCConfig/PLCConfig.hpp>
namespace plc {
namespace plc_asm {
namespace x_86 {
class AsmGenerator86 : public plc::plc_asm::AsmGenerator {
public:
  AsmGenerator86(plc::ir::IR Ir, const plc::Config &Config);
  void generateAssembly() override;
};

}
}
}