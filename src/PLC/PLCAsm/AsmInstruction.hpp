#pragma once
#include <string>
#include <vector>
namespace plc {
namespace plc_asm {
class AsmInstruction {
public:
  AsmInstruction(std::string Opcode, std::initializer_list<std::string> Args);
  ~AsmInstruction() = default;
  std::string str() const;
private:
  std::string m_Opcode; // opcode
  std::vector<std::string> m_Op; // operands
};
}
}