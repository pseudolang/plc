#include <PLC/PLCAsm/AsmInstruction.hpp>

plc::plc_asm::AsmInstruction::AsmInstruction(std::string Opcode, std::initializer_list<std::string> Args): m_Opcode(Opcode), m_Op({Args}) {}

std::string plc::plc_asm::AsmInstruction::str() const {
  std::string Operation(this->m_Opcode);
  if (this->m_Op.size() > 0 && !this->m_Op[0].empty()) {
	Operation += " " + this->m_Op[0];
	if (this->m_Op.size() > 1 && !this->m_Op[1].empty()) {
	  Operation += ", " + this->m_Op[1];
	  if (this->m_Op.size() > 2 && !this->m_Op[2].empty()) {
		Operation += ", " + this->m_Op[2];
	  }
	}
  }
  return Operation;
}
