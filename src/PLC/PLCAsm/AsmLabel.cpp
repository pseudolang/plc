#include <PLC/PLCAsm/AsmLabel.hpp>

plc::plc_asm::AsmLabel::AsmLabel(std::string Name) : m_Name(Name) {}

std::string plc::plc_asm::AsmLabel::getName() {
  return this->m_Name;
}
