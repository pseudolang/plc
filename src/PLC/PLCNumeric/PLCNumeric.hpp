#pragma once
#include <variant>
#include <string>
#include <cstdint>

namespace plc {
namespace numeric {

typedef std::variant<std::int8_t, // standard int types
					 std::int16_t,
					 std::int32_t,
					 std::int64_t,
					 std::uint8_t,
					 std::uint16_t,
					 std::uint32_t,
					 std::uint64_t,
					 std::string> PlcNumeric; // and string for other types

}
}