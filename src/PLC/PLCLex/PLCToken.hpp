#pragma once

#include <cstdint>
#include <string>
#include <variant>

namespace plc {
namespace lex {
enum TokenType {
  NO_TOKEN,
  FILE_END_TOKEN,
  ERROR_TOKEN,
  IDENTIFIER_TOKEN,
  CONSTANT_STRING_TOKEN,
  CONSTANT_NUMERIC_TOKEN,
  COLON_TOKEN,
  PARENTHESIS_OPENED_TOKEN,
  PARENTHESIS_CLOSED_TOKEN
};

typedef std::variant<std::nullptr_t, std::string> TokenData;

struct Token {
  Token(TokenType Type, TokenData Data) : type(Type), data(Data) {}
  TokenType type;
  TokenData data;
};
}
}
