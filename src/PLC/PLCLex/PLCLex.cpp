#include <PLC/PLCLex/PLCLex.hpp>
#include <cstring>
#include <PLC/PLCLog/PLCLog.hpp>

plc::lex::Lexer::Lexer(std::istream &Input) : m_Input(Input) {
}

void plc::lex::Lexer::run() {
  char Buffer[2 * BUFSIZ];
  m_Input.read(Buffer, sizeof Buffer);
  std::streamsize ReadChars = m_Input.gcount();
  std::streamsize BufCursor = 0;

  while (BufCursor < ReadChars) {
	plc::lex::Token Token = this->parseToken(Buffer, BufCursor, ReadChars, BUFSIZ);
	if (Token.type == plc::lex::TokenType::NO_TOKEN) {
	  BufCursor -= BUFSIZ;
	  ReadChars -= BUFSIZ;
	  std::memcpy(Buffer + BUFSIZ, Buffer, static_cast<size_t>(ReadChars));
	  m_Input.read(Buffer + BUFSIZ, BUFSIZ);
	} else if (Token.type == plc::lex::TokenType::ERROR_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Cannot understand");
	  return;
	} else {
	  m_Tokens.push_back(Token);
	  if (Token.type == plc::lex::TokenType::FILE_END_TOKEN)
		break;
	}
  }
#if DEBUG
  for (std::vector<plc::lex::Token>::iterator It = m_Tokens.begin(); It != m_Tokens.end(); It++) {
	if (It->type == plc::lex::TokenType::IDENTIFIER_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Identifier token: \"%s\"", std::get<std::string>(It->data).c_str());
	} else if (It->type == plc::lex::TokenType::CONSTANT_STRING_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Constant string token: \"%s\"", std::get<std::string>(It->data).c_str());
	} else if (It->type == plc::lex::TokenType::CONSTANT_NUMERIC_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Constant numeric token: %s", std::get<std::string>(It->data).c_str());
	} else if (It->type == plc::lex::TokenType::PARENTHESIS_OPENED_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Parenthesis opened token");
	} else if (It->type == plc::lex::TokenType::PARENTHESIS_CLOSED_TOKEN) {
	  plc::log(plc::LogSeverity::INFO, "Parenthesis closed token");
	}
  }
#endif
}

plc::lex::Token plc::lex::Lexer::parseToken(char *Buffer,
											std::streamsize &Cursor,
											std::streamsize ReadCount,
											std::streamsize SectorSize) {
  while (Cursor < ReadCount && Cursor < SectorSize && isspace(Buffer[Cursor])) {
	Cursor++;
  }
  if (Cursor == ReadCount) {
	return plc::lex::Token(plc::lex::TokenType::FILE_END_TOKEN, nullptr);
  }
  if (Cursor == SectorSize) {
	return plc::lex::Token(plc::lex::TokenType::NO_TOKEN, nullptr);
  }
  if (Buffer[Cursor] == ',') {
	++Cursor;
	return plc::lex::Token(plc::lex::TokenType::COLON_TOKEN, nullptr);
  } else if (Buffer[Cursor] == '(') {
	++Cursor;
	return plc::lex::Token(plc::lex::TokenType::PARENTHESIS_OPENED_TOKEN, nullptr);
  } else if (Buffer[Cursor] == ')') {
	++Cursor;
	return plc::lex::Token(plc::lex::TokenType::PARENTHESIS_CLOSED_TOKEN, nullptr);
  } else if (isalpha(Buffer[Cursor]) || Buffer[Cursor] == '_') {
	std::string S = "";
	while (isalnum(Buffer[Cursor]) || Buffer[Cursor] == '_') {
	  S.push_back(Buffer[Cursor]);
	  Cursor++;
	}

	return plc::lex::Token(plc::lex::TokenType::IDENTIFIER_TOKEN, S);
  } else if (Buffer[Cursor] == '"') {
	++Cursor; // skip the '"'
	std::string S;
	do {
	  S.push_back(Buffer[Cursor]);
	} while (Buffer[++Cursor] != '"');
	++Cursor; // skip the second '"'
	return plc::lex::Token(plc::lex::TokenType::CONSTANT_STRING_TOKEN, S);
  } else if (isdigit(Buffer[Cursor])) {
	std::string S; // keep it as a string until actual compilation
	// the parser should not concern itself with the type of number this is; knowing it is a number is enough
	S.reserve(30);
	if (Buffer[Cursor] == '0') { // oct or hex
	  if (Cursor + 1 < ReadCount && Buffer[Cursor + 1] == 'x') { // hex
		S.push_back(Buffer[Cursor++]); // push '0'
		S.push_back(Buffer[Cursor++]); // push 'x'
		while (Cursor < ReadCount && (isdigit(Buffer[Cursor]) || (Buffer[Cursor] >= 'a' && Buffer[Cursor] <= 'f')
			|| (Buffer[Cursor] >= 'A' && Buffer[Cursor] <= 'F'))) {
		  S.push_back(Buffer[Cursor]);
		  ++Cursor;
		}
	  } else { // oct
		S.push_back(Buffer[Cursor++]); // push '0'
		while (Cursor < ReadCount && Buffer[Cursor] >= '0' && Buffer[Cursor] <= '7') {
		  S.push_back(Buffer[Cursor]);
		  ++Cursor;
		}
	  }
	} else {
	  while (Cursor < ReadCount && isdigit(Buffer[Cursor])) {
		S.push_back(Buffer[Cursor]);
		++Cursor;
	  }
	}
	return plc::lex::Token(plc::lex::TokenType::CONSTANT_NUMERIC_TOKEN, S);
  }

  return plc::lex::Token(plc::lex::TokenType::ERROR_TOKEN, nullptr);
}

std::vector<plc::lex::Token> &plc::lex::Lexer::getTokens() {
  return m_Tokens;
}
