#pragma once

#include <istream>
#include <vector>
#include <PLC/PLCLex/PLCToken.hpp>

namespace plc {
namespace lex {
class Lexer {
public:
  Lexer(std::istream &Input);
  void run();
  std::vector<Token>& getTokens();
private:
  Token parseToken(char *Buffer, std::streamsize &Cursor, std::streamsize ReadCount, std::streamsize SectorSize);
  std::basic_istream<char> &m_Input;
  std::vector<Token> m_Tokens;
};
}
}
