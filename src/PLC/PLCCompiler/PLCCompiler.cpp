#include <PLC/PLCLog/PLCLog.hpp>
#include <PLC/PLCIR/PLCIR.hpp>
#include <PLC/PLCCompiler/PLCCompiler.hpp>

plc::compiler::Compiler::Compiler(plc::ast::Node *RootNode) {
  if (RootNode == nullptr) {
	plc::log(plc::LogSeverity::FATAL, "root node null");
	return;
  }
  m_TreeRootNode = RootNode;
}

void plc::compiler::Compiler::compile() {
  plc::ast::Node RootNode = *m_TreeRootNode;
  std::shared_ptr<plc::ast::Node> Child = RootNode.getFirstChild();
  plc::ir::IR Ir;
  while (Child != nullptr) {
	if (Child->getNodeType() == plc::ast::NodeType::FUNCTION_CALL_NODE) {
	  std::shared_ptr<plc::ast::Node> FunctionCallNameNode = Child->getFirstChild();
	  if (FunctionCallNameNode == nullptr
		  || FunctionCallNameNode->getNodeType() != plc::ast::NodeType::FUNCTION_CALL_NAME_NODE) {
		this->error("first child of FunctionCallNode is not FUNCTION_CALL_NAME_NODE");
		return;
	  }
	  std::shared_ptr<plc::ast::Node> FunctionArgs = FunctionCallNameNode->getNextSibling();
	  if (FunctionArgs == nullptr || FunctionArgs->getNodeType() != plc::ast::NodeType::PARENTHESIS_NODE) {
		this->error("second child of FunctionCallNode is not FUNCTION_CALL_ARG_NODE");
		return;
	  }
	  if (std::get<std::string>(FunctionCallNameNode->getToken()->data) == "write_string") {
		std::shared_ptr<plc::ast::Node> Arg = FunctionArgs->getFirstChild()->getNextSibling();
		Ir.pushInstr(plc::ir::Instruction(plc::ir::InstrEnum::LOAD_STR_CONST,
										  0,
										  Ir.addConstString(std::get<std::string>(Arg->getToken()->data))));
		Ir.pushInstr(plc::ir::Instruction(plc::ir::InstrEnum::API_CALL, 0, "write_string"));
	  }
	}
	Child = Child->getNextSibling();
  }
  this->m_Ir = Ir;
}

void plc::compiler::Compiler::error(std::string ErrorMessage) {
  plc::log(plc::LogSeverity::ERROR, "%s", ErrorMessage.c_str());
}

plc::ir::IR plc::compiler::Compiler::getIr() {
  return this->m_Ir;
}
