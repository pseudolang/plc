#pragma once

#include <PLC/PLCAST/PLCASTNode.hpp>
#include <PLC/PLCIR/PLCIR.hpp>

namespace plc {
namespace compiler {

class Compiler {
public:
  Compiler(plc::ast::Node *RootNode);
  void compile();
  plc::ir::IR getIr();
private:
  plc::ast::Node *m_TreeRootNode;
  void error(std::string ErrorMessage);
  plc::ir::IR m_Ir;
};

}
}