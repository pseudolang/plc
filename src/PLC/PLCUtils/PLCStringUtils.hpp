#pragma once

#include <string>

namespace plc {
std::string &trimString(std::string &S);

size_t parseVersion(const std::string &S);
}
