#include <PLC/PLCUtils/PLCStringUtils.hpp>

std::string &plc::trimString(std::string &S) {
  size_t Begin = S.find_first_not_of(" \n\t\r");
  size_t End = S.find_last_not_of(" \n\t\r");
  S = S.substr(Begin, End - Begin + 1);
  return S;
}

size_t plc::parseVersion(const std::string &S) {
  size_t FirstSep = S.find('.');
  size_t SecondSep = S.rfind('.');
  std::string MajorVString = S.substr(0, FirstSep);
  std::string MinorVString = S.substr(FirstSep + 1, SecondSep - FirstSep - 1);
  std::string PatchVString = S.substr(SecondSep + 1);
  return static_cast<size_t>(std::stoi(MajorVString) * 10000 + std::stoi(MinorVString) * 100
	  + std::stoi(PatchVString));
}
