#include <PLC/PLCLog/PLCLog.hpp>
#include <iostream>
#include <ctime>
#include <cstdarg>

void plc::log(plc::LogSeverity Severity, const char *const Message, ...) {
#if RELEASE
  if(Severity == LogSeverity::INFO) return;
#endif
  std::string SeverityStr = "";
  switch (Severity) {
  case INFO: {
	SeverityStr = "[INFO]";
	break;
  }
  case WARN: {
	SeverityStr = "[WARN]";
	break;
  }
  case ERROR: {
	SeverityStr = "[ERROR]";
	break;
  }
  case FATAL: {
	SeverityStr = "[FATAL]";
	break;
  }
  }
  time_t RawTime;
  struct tm *TimeInfo;
  char TimeBuffer[80];

  time(&RawTime);
  TimeInfo = localtime(&RawTime);

  strftime(TimeBuffer, sizeof(TimeBuffer), "%d-%m-%Y %H:%M:%S", TimeInfo);
  char Buffer[1024];
  {
	va_list List;
	va_start(List, Message);
	vsnprintf(Buffer, sizeof Buffer, Message, List);
	va_end(List);
  }
  std::cerr << SeverityStr << "[" << TimeBuffer << "]: " << Buffer << std::endl;
}
