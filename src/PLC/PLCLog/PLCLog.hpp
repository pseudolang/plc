#pragma once
#include <string>

namespace plc {

enum LogSeverity {
  INFO, WARN, ERROR, FATAL
};

void log(LogSeverity Severity, const char *const Message, ...)
#if defined(__GNUC__) || defined(__clang__)
__attribute__((format(printf, 2, 3)))
#endif
;
}
