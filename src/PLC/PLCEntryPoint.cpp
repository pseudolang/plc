#include <iostream>
#include <PLC/PLCLog/PLCLog.hpp>
#include <PLC/PLCConfig/PLCConfig.hpp>
#include <cstring>
#include <PLC/PLCLex/PLCLex.hpp>
#include <PLC/PLCParser/PLCParser.hpp>
#include <PLC/PLCCompiler/PLCCompiler.hpp>
#include <PLC/PLCAsm/64/AsmGenerator64.hpp>
#include <PLC/PLCAsm/64/AsmWriter64.hpp>
#include <PLC/PLCAsm/86/AsmWriter86.hpp>
#include <PLC/PLCAsm/86/AsmGenerator86.hpp>

int main(int Argc, char **Argv) {
  std::fstream ConfigIn;
#if defined(__linux__)
  ConfigIn.open("plc.conf", std::ios_base::in);
#elif defined(_WIN32)
  ConfigIn.open("plcwin.conf", std::ios_base::in);
#endif
  if (!ConfigIn.is_open()) {
	plc::log(plc::LogSeverity::FATAL, "cannot open config: %s", std::strerror(errno));
	return 1;
  }
  std::shared_ptr<plc::Config> Config = plc::parseConfig(ConfigIn);

  plc::log(plc::LogSeverity::INFO,
		   "PLC Compiling With Language Specification Version %zu.%zu.%zu",
		   Config->getLangMajorVersion(),
		   Config->getLangMinorVersion(),
		   Config->getLangPatchLevel());

  plc::parseConfigLineArgs(Config, Argc, Argv);
  std::fstream Source("test.psl", std::ios::in | std::ios::binary);
  plc::lex::Lexer Lexer(Source);
  Lexer.run();
  plc::parser::Parser Parser(Lexer);
  Parser.parse();
  plc::compiler::Compiler Compiler(Parser.getRootNode());
  Compiler.compile();

  plc::plc_asm::x_64::AsmGenerator64 AsmGen(Compiler.getIr(), *Config);
  AsmGen.generateAssembly();

  std::fstream Output("output.s", std::ios::out | std::ios::binary);
  plc::plc_asm::x_64::AsmWriter64 Writer(AsmGen, Output, *Config);
  Writer.write();

  return 0;
}
