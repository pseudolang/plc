#include <PLC/PLCLog/PLCLog.hpp>
#include <stack>
#include <PLC/PLCParser/PLCParser.hpp>

plc::parser::Parser::Parser(plc::lex::Lexer &Lexer) : m_Lexer(Lexer) {
}

void plc::parser::Parser::parse() {
  this->createTree();
  this->detailTree();
}

plc::ast::Node *plc::parser::Parser::getRootNode() {
  return this->m_ParseTree.get();
}
void plc::parser::Parser::createTree() {
  std::vector<plc::lex::Token> Tokens = m_Lexer.getTokens();
  this->m_ParseTree = std::make_shared<plc::ast::Node>(plc::ast::NodeType::SCOPE_NODE);
  std::stack<std::shared_ptr<plc::ast::Node> > NodesStack({this->m_ParseTree});
  for (size_t I = 0; I < Tokens.size(); I++) {
	if (Tokens[I].type == plc::lex::TokenType::IDENTIFIER_TOKEN) {
	  std::shared_ptr<plc::ast::Node>
		  NewNode = std::make_shared<plc::ast::Node>(plc::ast::NodeType::IDENTIFIER_NODE, Tokens[I]);
	  NodesStack.top()->addChild(NewNode);
	  NodesStack.push(NewNode);
	} else if (Tokens[I].type == plc::lex::TokenType::PARENTHESIS_OPENED_TOKEN) {
	  std::shared_ptr<plc::ast::Node>
		  ParOpen = std::make_shared<plc::ast::Node>(plc::ast::NodeType::PARENTHESIS_OPENED_NODE, Tokens[I]);
	  std::shared_ptr<plc::ast::Node> NewNode = std::make_shared<plc::ast::Node>(plc::ast::NodeType::PARENTHESIS_NODE);
	  NewNode->addChild(ParOpen);
	  NodesStack.pop();
	  NodesStack.top()->addChild(NewNode);
	  NodesStack.push(NewNode);
	} else if (Tokens[I].type == plc::lex::TokenType::PARENTHESIS_CLOSED_TOKEN) {
	  while (!NodesStack.empty() && NodesStack.top()->getNodeType() != plc::ast::NodeType::PARENTHESIS_NODE) {
		NodesStack.pop();
	  }
	  if (NodesStack.empty()) {
		plc::log(plc::LogSeverity::ERROR, "No matching '(' for ')'");
		return;
	  }
	  NodesStack.top()
		  ->addChild(std::make_shared<plc::ast::Node>(plc::ast::NodeType::PARENTHESIS_CLOSED_NODE, Tokens[I]));
	  NodesStack.pop();
	} else if (Tokens[I].type == plc::lex::TokenType::CONSTANT_STRING_TOKEN) {
	  NodesStack.top()->addChild(std::make_shared<plc::ast::Node>(plc::ast::NodeType::CONSTANT_STRING_NODE, Tokens[I]));
	}
  }
}

void plc::parser::Parser::detailTree() {
  std::shared_ptr<plc::ast::Node> Node = this->m_ParseTree->getFirstChild();
  while (Node != nullptr) {
	std::shared_ptr<plc::ast::Node> NextNode = Node->getNextSibling();
	if (Node->getNodeType() == plc::ast::NodeType::IDENTIFIER_NODE && NextNode != nullptr
		&& NextNode->getNodeType() == plc::ast::NodeType::PARENTHESIS_NODE) {
	  Node->setType(plc::ast::NodeType::FUNCTION_CALL_NAME_NODE);
	  NextNode = NextNode->getNextSibling();
	  Node->parentify(std::make_shared<plc::ast::Node>(plc::ast::NodeType::FUNCTION_CALL_NODE), 1);
	}
	Node = NextNode;
  }

#if DEBUG
  this->m_ParseTree->print();
#endif
}
