#pragma once

#include <PLC/PLCLex/PLCLex.hpp>
#include <PLC/PLCAST/PLCASTNode.hpp>

namespace plc {
namespace parser {

class Parser {
public:
  Parser(plc::lex::Lexer &Lexer);

  void parse();

  plc::ast::Node *getRootNode();
private:
  plc::lex::Lexer &m_Lexer;
  std::shared_ptr<plc::ast::Node> m_ParseTree;

  void createTree();
  void detailTree();
};

}
}