#pragma once

#include <cstdint>
#include <variant>
#include <vector>
#include <string>
#include <map>
#include <PLC/PLCIR/PLCVariable.hpp>
#include <PLC/PLCIR/PLCConstString.hpp>
namespace plc {
namespace ir {

enum InstrEnum {
  LOAD_STR_CONST, API_CALL,
};

class Instruction {
public:
  Instruction() : m_Created(false) {}
  template<typename ...ArgsT> Instruction(InstrEnum Instr, uint32_t Flags, ArgsT ... Args)
	  : m_Created(true), m_Instr(Instr), m_Flags(Flags) {
	for (auto Arg : {Args...}) {
	  m_Args.push_back(Arg);
	}
  }
  bool isCreated();
  InstrEnum getInstr() const;
  uint32_t getFlags() const;
  const std::vector<std::variant<std::string> > &getArgs() const;
private:
  bool m_Created;
  InstrEnum m_Instr;
  uint32_t m_Flags;
  std::vector<std::variant<std::string> > m_Args;
};

class IR {
public:
  IR();

  void pushInstr(Instruction Instruction);
  std::vector<plc::ir::Instruction> &getInstructions();

  // function that registers str as a constant string and returns the name it will use in assembly
  std::string addConstString(std::string Cs);

  size_t constStringLen(std::string Cs);

  const std::vector<plc::ir::ConstString> &getConstStrings() const;

private:
  std::vector<plc::ir::Instruction> m_Instructions;
  std::map<std::string, plc::ir::Variable> m_Variables;
  std::vector<plc::ir::ConstString> m_ConstStrings;
  std::string newConstString(std::string Str);
};

}
}