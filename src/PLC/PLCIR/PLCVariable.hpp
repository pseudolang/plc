#pragma once

#include <string>
namespace plc {
namespace ir {
enum VarType {
  INT_8, UINT_8, INT_16, UINT_16, INT_32, UINT_32, INT_64, UINT_64, STRING
};

class Variable {
public:
  Variable(std::string Name, VarType Type);
  std::string getName();
  VarType getType();
private:
  std::string m_Name;
  VarType m_Type;
};
}
}