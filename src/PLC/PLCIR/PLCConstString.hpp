#pragma once

#include <string>
namespace plc {
namespace ir {
class ConstString {
public:
  ConstString(std::string Name, std::string Value);

  std::string getName() const;
  std::string getValue() const;

  size_t computeLen() const;
private:
  std::string m_Name;
  std::string m_Value;
};
}
}