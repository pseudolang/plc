#include "PLCConstString.hpp"
plc::ir::ConstString::ConstString(std::string Name, std::string Value) : m_Name(Name), m_Value(Value) {

}
std::string plc::ir::ConstString::getName() const {
  return this->m_Name;
}
std::string plc::ir::ConstString::getValue() const {
  return this->m_Value;
}

size_t plc::ir::ConstString::computeLen() const {
  std::string S = this->m_Value;
  size_t I = 0;
  size_t Len = 0;
  while (I < S.length()) {
	if(S[I] == '\\'){
	  I++;
	}
	Len++;
	I++;
  }
  return Len;
}
