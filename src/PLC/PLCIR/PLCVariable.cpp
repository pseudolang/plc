#include <PLC/PLCIR/PLCVariable.hpp>

plc::ir::Variable::Variable(std::string Name, plc::ir::VarType Type) : m_Name(Name), m_Type(Type) {}

std::string plc::ir::Variable::getName() {
  return this->m_Name;
}

plc::ir::VarType plc::ir::Variable::getType() {
  return this->m_Type;
}
