#include <PLC/PLCIR/PLCIR.hpp>
#include <algorithm>

plc::ir::IR::IR() {}

void plc::ir::IR::pushInstr(plc::ir::Instruction Instruction) {
  this->m_Instructions.push_back(Instruction);
}
std::vector<plc::ir::Instruction> &plc::ir::IR::getInstructions() {
  return this->m_Instructions;
}
std::string plc::ir::IR::addConstString(std::string Str) {
  std::vector<plc::ir::ConstString>::iterator Loc =
	  std::find_if(this->m_ConstStrings.begin(), this->m_ConstStrings.end(), [Str](plc::ir::ConstString Cs) -> bool {
		return Str == Cs.getValue();
	  });
  if (Loc != this->m_ConstStrings.end()) {
	return Loc->getName();
  }
  return this->newConstString(Str);
}
size_t plc::ir::IR::constStringLen(std::string Name) {
  std::vector<plc::ir::ConstString>::iterator Loc =
	  std::find_if(this->m_ConstStrings.begin(), this->m_ConstStrings.end(), [Name](plc::ir::ConstString Cs) -> bool {
		return Name == Cs.getName();
	  });
  if (Loc != this->m_ConstStrings.end()) {
	return Loc->computeLen();
  }
  return 0;
}
std::string plc::ir::IR::newConstString(std::string Str) {
  ConstString s("gs" + std::to_string(this->m_ConstStrings.size()), Str);
  this->m_ConstStrings.push_back(s);
  return s.getName();
}
const std::vector<plc::ir::ConstString> &plc::ir::IR::getConstStrings() const {
  return this->m_ConstStrings;
}
plc::ir::InstrEnum plc::ir::Instruction::getInstr() const {
  return this->m_Instr;
}
uint32_t plc::ir::Instruction::getFlags() const {
  return this->m_Flags;
}
const std::vector<std::variant<std::string> > &plc::ir::Instruction::getArgs() const {
  return this->m_Args;
}
bool plc::ir::Instruction::isCreated() {
  return this->m_Created;
}
